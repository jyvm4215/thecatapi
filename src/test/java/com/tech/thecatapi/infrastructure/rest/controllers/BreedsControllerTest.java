package com.tech.thecatapi.infrastructure.rest.controllers;

import com.tech.thecatapi.application.services.BreedService;
import com.tech.thecatapi.domain.models.Breed;
import com.tech.thecatapi.util.mock.BreedMock;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(BreedsController.class)
class BreedsControllerTest {


    private final static String BASE_URL = "/api/v1/breeds";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BreedService breedService;
    @Test
    public void test_response_ok() throws Exception {
        List<Breed> breedMock = BreedMock.getAllBreeds();

        when(breedService.getAllBreeds(any(), any()))
                .thenReturn(breedMock);

        mockMvc.perform(get(BASE_URL).contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value("abys"));

        verify(breedService).getAllBreeds(null, null);
    }

    @Test
    public void test_search_empty() throws Exception {
        String apiUrl = BASE_URL + "/search";
        Map<String, String> query = new HashMap<String, String>();
        query.put("q", "xairx");
        query.put("attach_image", "1");
        List<Breed> breedMock = BreedMock.getBreedBySearch(query.get("q"), Long.parseLong(query.get("attach_image")));

        when(breedService.getBreedBySearch(anyString(), anyLong()))
                .thenReturn(breedMock);

        mockMvc.perform(get(apiUrl).contentType(MediaType.APPLICATION_JSON)
                        .param("q", query.get("q"))
                        .param("attach_image", query.get("attach_image")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0]").doesNotExist());

        verify(breedService).getBreedBySearch(query.get("q"), Long.parseLong(query.get("attach_image")));
    }

    @Test
    public void test_id_ok() throws Exception {
        String breedId = "abys";
        String apiUrl = BASE_URL + "/" + breedId;
        Optional<Breed> breedMock = BreedMock.getBreedById(breedId);

        when(breedService.getBreedById(any()))
                .thenReturn(breedMock);

        mockMvc.perform(get(apiUrl).contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("abys"));

        verify(breedService).getBreedById(breedId);
    }

    @Test
    public void test_id_not_found() throws Exception {
        String breedId = "notfound";
        String apiUrl = BASE_URL + "/" + breedId;
        Optional<Breed> breedMock = BreedMock.getBreedById(breedId);

        when(breedService.getBreedById(any()))
                .thenReturn(breedMock);

        mockMvc.perform(get(apiUrl).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void test_search_ok() throws Exception {
        String apiUrl = BASE_URL + "/search";
        Map<String, String> params = new HashMap<String, String>();
        params.put("q", "air");
        params.put("attach_image", "1");
        List<Breed> breedMock = BreedMock.getBreedBySearch(params.get("q"), Long.parseLong(params.get("attach_image")));

        when(breedService.getBreedBySearch(anyString(), anyLong()))
                .thenReturn(breedMock);

        mockMvc.perform(get(apiUrl).contentType(MediaType.APPLICATION_JSON)
                        .param("q", params.get("q"))
                        .param("attach_image", params.get("attach_image")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value(Matchers.containsString(params.get("q"))));

        verify(breedService).getBreedBySearch(params.get("q"), Long.parseLong(params.get("attach_image")));
    }

    @Test
    public void test_search_bad_request() throws Exception {
        String apiUrl = BASE_URL + "/search";
        String messageExpected = "Required request parameter 'q' for method parameter type String is not present";
        Map<String, String> params = new HashMap<String, String>();
        params.put("attach_image", "1");

        mockMvc.perform(get(apiUrl).contentType(MediaType.APPLICATION_JSON)
                        .param("attach_image", params.get("attach_image")))
                .andExpect(status().isBadRequest())
                .andExpect(
                        result -> assertTrue(result.getResolvedException() instanceof MissingServletRequestParameterException))
                .andExpect(result -> assertEquals(messageExpected, result.getResolvedException().getMessage()));
    }



    @Test
    public void test_response_not_found() throws Exception {
        String apiUrl = BASE_URL + "x";
        String messageExpected = "No static resource " + apiUrl.substring(1) + ".";

        mockMvc.perform(get(apiUrl).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof NoResourceFoundException))
                .andExpect(result -> assertEquals(messageExpected, result.getResolvedException().getMessage()));
    }

}