package com.tech.thecatapi.application.usecases;

import com.tech.thecatapi.domain.models.Breed;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;


public interface IBreedService {

    List<Breed> getAllBreeds(Long limit, Long page);

    Optional<Breed> getBreedById(String id) throws IOException;

    List<Breed> getBreedBySearch(String query, Long attachImage);
}
