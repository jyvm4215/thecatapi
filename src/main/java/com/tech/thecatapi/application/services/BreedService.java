package com.tech.thecatapi.application.services;

import com.tech.thecatapi.application.usecases.IBreedService;
import com.tech.thecatapi.domain.models.Breed;
import com.tech.thecatapi.domain.ports.out.ICatAPIServicePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class BreedService implements IBreedService {

    @Autowired
    private ICatAPIServicePort apiTheCatServicePort;


    public List<Breed> getAllBreeds(Long limit, Long page) {
        return apiTheCatServicePort.getAllBreeds(limit, page);
    }


    public Optional<Breed> getBreedById(String id) throws IOException {
        return apiTheCatServicePort.getBreedById(id);
    }


    public List<Breed> getBreedBySearch(String query, Long attachImage) {
        return apiTheCatServicePort.getBreedBySearch(query, attachImage);
    }
}
