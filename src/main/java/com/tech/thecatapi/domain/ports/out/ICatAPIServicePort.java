package com.tech.thecatapi.domain.ports.out;

import com.tech.thecatapi.domain.models.Breed;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public interface ICatAPIServicePort {

    List<Breed> getAllBreeds(Long limit, Long page);

    Optional<Breed> getBreedById(String id);

    List<Breed> getBreedBySearch(String query, Long attachImage);
}
