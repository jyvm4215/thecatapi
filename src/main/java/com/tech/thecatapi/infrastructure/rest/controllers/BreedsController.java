package com.tech.thecatapi.infrastructure.rest.controllers;

import com.tech.thecatapi.application.services.BreedService;
import com.tech.thecatapi.application.usecases.IBreedService;
import com.tech.thecatapi.domain.models.Breed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/breeds")
public class BreedsController  {

    @Autowired
    private final IBreedService breedService;

    public BreedsController(IBreedService breedService) {
        this.breedService = breedService;
    }

    @GetMapping
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<List<Breed>> getAllBreeds(@RequestParam(required = false) Long limit,
                                                    @RequestParam(required = false) Long page) {

        List<Breed> breeds = breedService.getAllBreeds(limit, page);
        return new ResponseEntity<>(breeds, HttpStatus.OK);
    }

    @GetMapping("/{breedId}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Breed> getBreedById(@PathVariable String breedId) {
        try {
            return breedService.getBreedById(breedId)
                    .map(breed -> new ResponseEntity<>(breed, HttpStatus.OK))
                    .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

        }catch (Exception exception){
            System.out.println("Error consultando por id " + exception.getMessage());
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error realizando consulta", exception);

        }

    }

    @GetMapping("/search")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<List<Breed>> getBreedBySearch(@RequestParam(required = true) String q,
                                                        @RequestParam(required = false) Long attach_image) {
        List<Breed> breeds = breedService.getBreedBySearch(q, attach_image);
        return new ResponseEntity<>(breeds, HttpStatus.OK);
    }
}
