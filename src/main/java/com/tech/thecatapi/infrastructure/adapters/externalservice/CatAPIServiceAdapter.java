package com.tech.thecatapi.infrastructure.adapters.externalservice;

import com.tech.thecatapi.domain.models.Breed;
import com.tech.thecatapi.domain.ports.out.ICatAPIServicePort;
import com.tech.thecatapi.util.QueryString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class CatAPIServiceAdapter implements ICatAPIServicePort {

    private final RestTemplate restTemplate;

    @Value("${application.apiTheCat.xApiKey}")
    private String TOKEN;

    @Value("${application.apiTheCat.urlApi}")
    private String URL_API;

    public CatAPIServiceAdapter() {
        this.restTemplate = new RestTemplate();
    }

    @Override
    public List<Breed> getAllBreeds(Long limit, Long page) {
        QueryString queryParam = new QueryString();

        if (limit != null) {
            queryParam.add("limit", Long.toString(limit));
        }

        if (page != null) {
            queryParam.add("page", Long.toString(page));
        }


        return getCatAPI(null, queryParam.toString());
    }

    @Override
    public Optional<Breed> getBreedById(String id)  {
        return getCatAPI(id);
    }

    @Override
    public List<Breed> getBreedBySearch(String query, Long attachImage) {
        QueryString queryParam = new QueryString();

        if (query != null) {
            queryParam.add("q", query);
        }

        if (attachImage != null) {
            queryParam.add("attach_image", Long.toString(attachImage));
        }

        return getCatAPI("search", queryParam.toString());
    }


    private Optional<Breed> getCatAPI(String id)  {
        ResponseEntity<Breed> breedResponse = restTemplate.exchange(getApiUrl(id, (String) null), HttpMethod.GET,
                getHttpEntity(), Breed.class);

        return Optional.ofNullable(breedResponse.getBody());
    }

    private List<Breed> getCatAPI(String path, String queryString) {
        ResponseEntity<Breed[]> breedsResponse = restTemplate.exchange(getApiUrl(path, queryString), HttpMethod.GET,
                getHttpEntity(),
                Breed[].class);
        Breed[] breedsObject = breedsResponse.getBody();

        return Arrays.asList(breedsObject);
    }

    private HttpEntity<String> getHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("x-api-key", TOKEN);

        return new HttpEntity<String>(headers);
    }

    private String getApiUrl(String path, String... queryString) {
        String apiUrl = URL_API + "/breeds";
        if (path != null) {
            apiUrl += "/" + path;
        }
        if (queryString != null) {
            apiUrl += "?" + queryString[0];
        }

        System.out.println(apiUrl);
        System.out.println(Arrays.toString(queryString));

        return apiUrl;
    }
}
