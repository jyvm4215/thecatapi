FROM openjdk:17-jdk-alpine
COPY build/libs/thecatapi-0.0.1-SNAPSHOT.jar thecat-app.jar
ENTRYPOINT ["java", "-jar", "thecat-app.jar"]

